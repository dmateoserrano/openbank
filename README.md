# PASSWORD MANAGER

Esta prueba técnica consiste en crear un sistema ficticio que permita al usuario poder crear una colección de constraseñas para ayudarle en la gestión de las mismas.

## **¿Cómo iniciar el proyecto?**

En primer lugar debemos seguir los siguiente pasos para poder construir el proyecto con éxito.

### **Instalar dependencias**

En la raíz del proyecto ejecutar:

```bash
npm install
```

### **Construir y arrancar el proyecto**

```bash
npm start
```

### **Pasar los test del proyecto**

```bash
npm test
```

## **¿Cómo funciona?**

Cuando terminan de instalarse las dependencias de node y ha construido correctamente se abre automáticamente en el navegador predeterminado con la pantalla de bienvenida al sistema.

La aplicación soporta 2 idiomas (castellano e inglés), a su vez compuestos por 2 pasos que pueden ser accedidos con las siguientes urls:

- [http://localhost:3000?lng=es](http://localhost:3000?lng=es)
- [http://localhost:3000/form?lng=es](http://localhost:3000/form?lng=es)
- [http://localhost:3000?lng=en](http://localhost:3000?lng=en)
- [http://localhost:3000/form?lng=en](http://localhost:3000/form?lng=en)

> También se puede configurar en el localStorage del navegador la clave `i18nextLng` con valores `en` o `es`. En caso de tener configurado un idioma no soportado, se sirve por defecto castellano.

En el primero de los pasos podemos ver una información introductoria del producto. **Respecto al diseño original se ha eliminado el botón "Cancelar"** ya que he decidido que su funcionalidad sea redirigir al usuario al primer paso, lo cual parece que no tenía mucho sentido.

El segundo paso de la aplicación sirve para validar los passwords que se quieren almacenar en el sistema. Puede ser accedida directamente por la barra de direcciones del navegador, o bien pulsando el botón "Siguiente" si estamos en el paso 1.

Desde este segundo paso podemos:

- Pulsar el botón "Cancelar" para volver al paso 1.
- Pulsar el botón "Siguiente" para crear la nueva contraseña maestra. Una vez hecho, sucede lo siguiente:
  1. Mostramos al usuario un modal de espera mientras se está procesando la creación del password.
  2. Mostramos a continuación un modal de OK si todo ha ido bien. En caso contrario uno de KO.

## **Detalles de implementación**

Se han implementado los componentes de React de 2 formas distintas:

1. Con componentes React tradicionales, más enfocados a una programación orientada a objetos.
2. Con componentes funcionales (`<PasswordInput />`, `<ClueInput />` y `<ScreenTitle />`), con la finalidad de ilustrar que el desarrollador conoce el modo de trabajo con versiones de la librería actuales (p.ej React hooks).

Respecto a la validación de los inputs, el enfoque ha sido el siguiente:

1. Se muestra una barra de color dentro del input cuando se teclea siempre que la cadena introducida no cumple con los requisitos (entre 8 y 24 caracteres, y al menos un número y una letra mayúscula).
2. Una vez que la contraseña alcanza la longitud mínima, si el resto de criterios no se cumplen, se muestra debajo del input un mensaje de error.
3. El input de repetir el password funciona del mismo modo.
4. Si ya hay un password válido en uno de los 2 inputs, y el otro no cumple los criterios establecidos se muestra un mensaje de error distinto "Passwords distintos".
5. Si los 2 passwords cumplen la validación y son el mismo, se activa el botón "Siguiente" que nos permite darlo de alta.

## **Posibles mejoras que admite el proyecto**

A continuación enumero un conjunto de mejoras que se podrían aplicar aún al proyecto.

1. Preparar unas hojas css más sólidas para navegadores "vintage" (p.ej IE11).
   - Por ejemplo usando la libreria `styled-components`.
2. Añadir un catálogo de componentes para tener bien controlados los estados de los mismos.
   - Por ejemplo con la librería `Storybooks`, que nos permite cargar los componentes de una app de forma independiente para representar los estaods de los mismo (p.ej los distintos estados de los inputs de los passwords, o quizás las distintas reprensentaciones del componente `<FeedbackModal />`)
