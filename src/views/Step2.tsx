import { Component } from "react";
import { TFunction, withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import Header from "../components/Header";
import Footer from "../components/Footer";
import PasswordScreen from "../components/PasswordScreen";
import FeedbackModal from "../components/FeedbackModal";
import { submitForm } from "../services/api";

enum View {
  PASSWORD_SCREEN = "PASSWORD_SCREEN",
  SUCCESS_MODAL = "SUCCESS_MODAL",
  FAIL_MODAL = "FAIL_MODAL",
  LOADING_MODAL = "LOADING_MODAL",
}

interface Step2Props {
  t: TFunction;
}

interface Step2State {
  screen: View;
  password: string;
  clue: string;
  isValidationOk: boolean;
}

class Step2 extends Component<Step2Props, Step2State> {
  constructor(props: Step2Props) {
    super(props);
    this.handleRefreshValidation = this.handleRefreshValidation.bind(this);
    this.handleNextStep = this.handleNextStep.bind(this);
    this.state = {
      screen: View.PASSWORD_SCREEN,
      password: "",
      clue: "",
      isValidationOk: false,
    };
  }
  render() {
    const { t } = this.props;

    if (this.state.screen === View.PASSWORD_SCREEN) {
      return (
        <div id={"screen"}>
          <Header step={2} />
          <PasswordScreen onValidation={this.handleRefreshValidation} />
          <Footer>
            <Link to={{ pathname: "/" }} className="left">
              <button className="white" type="button">
                {t("footer.cancel")}
              </button>
            </Link>
            <button
              className="blue right"
              disabled={!this.state.isValidationOk}
              type="button"
              onClick={this.handleNextStep}
            >
              {t("footer.next")}
            </button>
          </Footer>
        </div>
      );
    }

    if (this.state.screen === View.LOADING_MODAL) {
      return <FeedbackModal isLoading />;
    }

    return <FeedbackModal isOk={this.state.screen === View.SUCCESS_MODAL} />;
  }
  handleRefreshValidation(
    password: string,
    clue: string,
    isValidationOk: boolean
  ) {
    this.setState({ password, clue, isValidationOk });
  }
  handleNextStep() {
    this.setState({ screen: View.LOADING_MODAL }, () =>
      submitForm(this.state.password)
        .then(() => this.setState({ screen: View.SUCCESS_MODAL }))
        .catch(() => this.setState({ screen: View.FAIL_MODAL }))
    );
  }
}

export default withTranslation()(Step2);
