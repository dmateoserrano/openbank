import { Component } from "react";
import { Link } from "react-router-dom";
import { TFunction, withTranslation } from "react-i18next";
import brain from "../assets/img/brain.png";
import securityBox from "../assets/img/security-box.png";
import Header from "../components/Header";
import Footer from "../components/Footer";
import ScreenTitle from "../components/ScreenTitle";

interface Step1Props {
  t: TFunction;
}

class Step1 extends Component<Step1Props> {
  render() {
    const { t } = this.props;

    return (
      <div id="screen">
        <Header step={1} />
        <main>
          <ScreenTitle title={t("screen.title")} />
          <section className="information">
            <img alt="shows a brain" src={brain} />
            <p>{`${t("step1.save-your-password")}.`}</p>
          </section>
          <section className="information">
            <img alt="shows a security box" src={securityBox} />
            <p>{`${t("step1.create-your-master-key")}.`}</p>
          </section>
          <article>
            <h5>{t("step1.how-it-works")}</h5>
            <p>{`${t("step1.remember-your-password")}.`}</p>
          </article>
          <article>
            <h5>{t("step1.which-data-can-you-store")}</h5>
            <p>{`${t("step1.you-can-store-pin-puk")}.`}</p>
          </article>
        </main>
        <Footer>
          <Link to={{ pathname: "/form" }} className="right">
            <button className="blue" type="button">
              {t("footer.next")}
            </button>
          </Link>
        </Footer>
      </div>
    );
  }
}

export default withTranslation()(Step1);
