import { BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import Step1 from "./Step1";
import config from "../locales/config";

i18n.use(initReactI18next).init(config);

describe("Step1", () => {
  beforeEach(() =>
    render(
      <Router>
        <Step1 />
      </Router>
    )
  );
  test("renders a title", () => {
    expect(screen.getByText("Crea tu Password Manager")).toBeInTheDocument();
  });
  test("renders a 'how it works' article", () => {
    expect(screen.getByText("Cómo funciona")).toBeInTheDocument();
  });
  test("renders a 'what can you store' article", () => {
    expect(screen.getByText("Qué datos puedes guardar")).toBeInTheDocument();
  });
  test("renders a next button", () => {
    expect(screen.getByText("Siguiente")).toBeInTheDocument();
  });
  test("does not renders a cancel button", () => {
    expect(screen.queryByText("Cancelar")).not.toBeInTheDocument();
  });
  test("allows to navigate from step1 to step 2", async () => {
    const nextButton = screen.getByText("Siguiente");
    fireEvent.click(nextButton);
    expect(window.location.href).toContain("/form");
  });
});
