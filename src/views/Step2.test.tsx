import { BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import Step2 from "./Step2";
import config from "../locales/config";

i18n.use(initReactI18next).init(config);

describe("Step2", () => {
  beforeEach(() =>
    render(
      <Router>
        <Step2 />
      </Router>
    )
  );
  test("renders a title", () => {
    expect(screen.getByText("Crea tu Password Manager")).toBeInTheDocument();
  });
  test("renders a password input", () => {
    expect(
      screen.getByPlaceholderText("Introduce tu contraseña")
    ).toBeInTheDocument();
  });
  test("renders a repeat password input", () => {
    expect(
      screen.getByPlaceholderText("Repite tu contraseña")
    ).toBeInTheDocument();
  });
  test("at first it renders a disabled next button", () => {
    expect(screen.getByText("Siguiente")).toBeDisabled();
  });
  test("keeps disabled at next button when passwords doesn't match", () => {
    const passwordInput = screen.getByPlaceholderText(
      "Introduce tu contraseña"
    );
    const repeatPasswordInput = screen.getByPlaceholderText(
      "Repite tu contraseña"
    );
    const validPasswordA = "aaaa7UaA";
    const validPasswordB = "aaaa7UaB";
    fireEvent.change(passwordInput, { target: { value: validPasswordA } });
    fireEvent.change(repeatPasswordInput, {
      target: { value: validPasswordB },
    });
    expect(screen.getByText("Siguiente")).toBeDisabled();
  });
  test("keeps disabled at next button when passwords are incorrect", () => {
    const passwordInput = screen.getByPlaceholderText(
      "Introduce tu contraseña"
    );
    const repeatPasswordInput = screen.getByPlaceholderText(
      "Repite tu contraseña"
    );
    const invalidPassword = "aaaa";
    fireEvent.change(passwordInput, { target: { value: invalidPassword } });
    fireEvent.change(repeatPasswordInput, {
      target: { value: invalidPassword },
    });
    expect(screen.getByText("Siguiente")).toBeDisabled();
  });
  test("keeps disabled at next button when password one is correct and password two one is incorrect", () => {
    const passwordInput = screen.getByPlaceholderText(
      "Introduce tu contraseña"
    );
    const repeatPasswordInput = screen.getByPlaceholderText(
      "Repite tu contraseña"
    );
    const validPassword = "aaaa7UaA";
    const invalidPassword = "aaaa";
    fireEvent.change(passwordInput, { target: { value: validPassword } });
    fireEvent.change(repeatPasswordInput, {
      target: { value: invalidPassword },
    });
    expect(screen.getByText("Siguiente")).toBeDisabled();
  });
  test("keeps disabled at next button when password one is incorrect and password two one is correct", () => {
    const passwordInput = screen.getByPlaceholderText(
      "Introduce tu contraseña"
    );
    const repeatPasswordInput = screen.getByPlaceholderText(
      "Repite tu contraseña"
    );
    const validPassword = "aaaa7UaA";
    const invalidPassword = "aaaa";
    fireEvent.change(passwordInput, { target: { value: invalidPassword } });
    fireEvent.change(repeatPasswordInput, {
      target: { value: validPassword },
    });
    expect(screen.getByText("Siguiente")).toBeDisabled();
  });
  test("removes disabled at next button when passwords are correct", () => {
    const passwordInput = screen.getByPlaceholderText(
      "Introduce tu contraseña"
    );
    const repeatPasswordInput = screen.getByPlaceholderText(
      "Repite tu contraseña"
    );
    const validPassword = "aaaa7UaA";
    fireEvent.change(passwordInput, { target: { value: validPassword } });
    fireEvent.change(repeatPasswordInput, { target: { value: validPassword } });
    expect(screen.getByText("Siguiente")).not.toHaveAttribute("disabled=''");
  });
  test("allows to navigate from step2 to step 1", async () => {
    const nextButton = screen.getByText("Cancelar");
    fireEvent.click(nextButton);
    expect(window.location.href).toMatch(/.+localhost\//);
  });
});
