import { Component } from "react";
import { TFunction, withTranslation } from "react-i18next";
import ClueInput from "./ClueInput";
import ScreenTitle from "./ScreenTitle";
import PasswordInput, { isValidPassword } from "./PasswordInput";

interface PasswordScreenProps {
  onValidation: (
    password: string,
    clue: string,
    hasGlobalValidation: boolean
  ) => void;
  t: TFunction;
}

interface PasswordScreenState {
  password: string;
  repeatedPassword: string;
  clue: string;
  isValid: boolean;
  isValidRepeated: boolean;
  hasFailedValidation: boolean;
}

class PasswordScreen extends Component<
  PasswordScreenProps,
  PasswordScreenState
> {
  constructor(props: PasswordScreenProps) {
    super(props);
    this.handleClueInput = this.handleClueInput.bind(this);
    this.handlePasswordInput = this.handlePasswordInput.bind(this);
    this.handleGlobalValidation = this.handleGlobalValidation.bind(this);
    this.handleRepeatedPasswordInput = this.handleRepeatedPasswordInput.bind(
      this
    );
    this.state = {
      password: "",
      repeatedPassword: "",
      clue: "",
      isValid: false,
      isValidRepeated: false,
      hasFailedValidation: false,
    };
  }
  render() {
    const { t } = this.props;
    return (
      <main>
        <ScreenTitle title={t("screen.title")} />
        <p>{`${t("step2.create-a-different-password")}.`}</p>
        <p>{`${t("step2.remember-your-password")}.`}</p>
        <PasswordInput
          title={t("step2.create-your-master-key")}
          placeholder={t("step2.insert-your-password")}
          errorLabel={this.renderErrorLabel()}
          value={this.state.password}
          isValid={this.state.isValid && !this.state.hasFailedValidation}
          onInput={this.handlePasswordInput}
        />
        <PasswordInput
          isRepetitionInput
          title={t("step2.repeat-your-master-key")}
          placeholder={t("step2.repeat-your-password")}
          errorLabel={this.renderErrorLabel()}
          value={this.state.repeatedPassword}
          isValid={
            this.state.isValidRepeated && !this.state.hasFailedValidation
          }
          onInput={this.handleRepeatedPasswordInput}
        />
        <p>{`${t("step2.you-can-create-a-clue")}.`}</p>
        <ClueInput value={this.state.clue} onInput={this.handleClueInput} />
      </main>
    );
  }
  renderErrorLabel() {
    return this.state.hasFailedValidation &&
      this.state.password !== this.state.repeatedPassword
      ? this.props.t("step2.not-matching-passwords")
      : this.props.t("step2.check-password-conditions");
  }
  handlePasswordInput(password: string) {
    this.setState(
      { password, isValid: isValidPassword(password) },
      this.handleGlobalValidation
    );
  }
  handleRepeatedPasswordInput(repeatedPassword: string) {
    this.setState(
      {
        repeatedPassword,
        isValidRepeated: isValidPassword(repeatedPassword),
      },
      this.handleGlobalValidation
    );
  }
  handleClueInput(clue: string) {
    this.setState({ clue });
  }
  handleGlobalValidation() {
    if (this.state.isValid && this.state.isValidRepeated) {
      this.setState(
        {
          hasFailedValidation:
            this.state.password !== this.state.repeatedPassword,
        },
        () =>
          this.props.onValidation(
            this.state.password,
            this.state.clue,
            this.state.password === this.state.repeatedPassword
          )
      );
    } else if (!this.state.isValid && !this.state.isValidRepeated) {
      this.setState({ hasFailedValidation: false }, () =>
        this.props.onValidation(this.state.password, this.state.clue, false)
      );
    } else if (
      !this.state.isValid &&
      this.state.isValidRepeated &&
      this.state.password.length >= this.state.repeatedPassword.length
    ) {
      this.setState({ hasFailedValidation: true }, () =>
        this.props.onValidation(this.state.password, this.state.clue, false)
      );
    } else if (
      this.state.isValid &&
      !this.state.isValidRepeated &&
      this.state.repeatedPassword.length >= this.state.password.length
    ) {
      this.setState({ hasFailedValidation: true }, () =>
        this.props.onValidation(this.state.password, this.state.clue, false)
      );
    }
  }
}

export default withTranslation()(PasswordScreen);
