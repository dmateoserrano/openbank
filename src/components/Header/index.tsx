import { Component } from "react";
import Step from "./Step";
import UnionStep from "./UnionStep";

export enum Steps {
  FIRST = 1,
  SECOND = 2,
}
interface HeaderProps {
  step: number;
}
class Header extends Component<HeaderProps> {
  render() {
    return (
      <header className={this.props.step === Steps.FIRST ? "step1" : "step2"}>
        <ul>
          <Step step={1} activeStep={this.props.step} />
          <UnionStep step={1} activeStep={this.props.step} />
          <Step step={2} activeStep={this.props.step} />
          <UnionStep step={2} activeStep={this.props.step} />
          <Step step={3} activeStep={this.props.step} />
        </ul>
        <div className="triangle" />
      </header>
    );
  }
}

export default Header;
