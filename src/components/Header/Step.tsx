import { Component } from "react";
import { Steps } from ".";

export interface StepProps {
  step: number;
  activeStep: number;
}

class Step extends Component<StepProps> {
  render() {
    return (
      <li className={this.renderStepClassName()}>
        <span>
          {this.props.activeStep === Steps.SECOND &&
          this.props.step === Steps.FIRST
            ? "✓"
            : this.props.step}
        </span>
      </li>
    );
  }
  renderStepClassName() {
    const { step, activeStep } = this.props;
    const BASE_CLASSNAME = "circle";

    if (step < activeStep) {
      return `${BASE_CLASSNAME} red`;
    }
    if (step > activeStep) {
      return `${BASE_CLASSNAME} disabled`;
    }
    return BASE_CLASSNAME;
  }
}

export default Step;
