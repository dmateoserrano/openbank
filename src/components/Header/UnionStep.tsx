import { Component } from "react";
import { StepProps } from "./Step";

const LAST_STEP: number = 5;
const isLastStep = (step: number) => step === LAST_STEP;

class UnionStep extends Component<StepProps> {
  render() {
    return <li className={this.renderStepClassName()} />;
  }
  renderStepClassName() {
    const { step, activeStep } = this.props;
    const BASE_CLASSNAME = "line";

    if (step < activeStep) {
      return `${BASE_CLASSNAME} red`;
    }

    if (!isLastStep(step) || step > activeStep) {
      return `${BASE_CLASSNAME} disabled`;
    }
    return BASE_CLASSNAME;
  }
}

export default UnionStep;
