import React, { FunctionComponent } from "react";
import { useTranslation } from "react-i18next";
import info from "../assets/img/info.png";

const MAX_CLUE_LENGTH: number = 60;

interface ClueInputProps {
  value: string;
  onInput: (clue: string) => void;
}

const ClueInput: FunctionComponent<ClueInputProps> = (props) => {
  const { t } = useTranslation();
  const handleUserInput = (
    event: React.KeyboardEvent<HTMLInputElement>
  ): void => props.onInput(event.currentTarget.value);

  return (
    <section className="clue">
      <div>
        <h5>{t("step2.create-a-clue-to-remember-your-password")}</h5>
        <img
          alt="more information icon"
          src={info}
          title={t("step2.you-can-create-clue")}
        />
      </div>
      <input
        type="text"
        defaultValue={props.value}
        maxLength={MAX_CLUE_LENGTH}
        onInput={handleUserInput}
      />
      <span>{`${props.value.length}/${MAX_CLUE_LENGTH}`}</span>
    </section>
  );
};

export default ClueInput;
