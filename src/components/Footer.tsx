import { Component, ReactElement } from "react";

interface FooterProps {
  children: ReactElement | ReactElement[];
}

class Footer extends Component<FooterProps> {
  render() {
    return <footer>{this.props.children}</footer>;
  }
}

export default Footer;
