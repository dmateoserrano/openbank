import { Fragment, FunctionComponent } from "react";

interface ScreenTitleProps {
  title: string;
}

const ScreenTitle: FunctionComponent<ScreenTitleProps> = (props) => (
  <Fragment>
    <h2>{props.title}</h2>
    <hr />
  </Fragment>
);

export default ScreenTitle;
