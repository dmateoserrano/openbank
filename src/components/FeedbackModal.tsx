import { Component } from "react";
import { withTranslation, TFunction } from "react-i18next";
import { Link } from "react-router-dom";
import loading from "../assets/img/loading.gif";
import success from "../assets/img/success.png";
import fail from "../assets/img/fail.png";
import Footer from "./Footer";

interface FeedbackModalProps {
  isOk?: boolean | false;
  isLoading?: boolean | false;
  t: TFunction;
}

class FeedbackModal extends Component<FeedbackModalProps> {
  constructor(props: FeedbackModalProps) {
    super(props);
    this.renderTitle = this.renderTitle.bind(this);
  }
  render() {
    const { t, isLoading, isOk } = this.props;
    return (
      <div id="modal">
        <main>
          <img
            className={isLoading ? "loading" : undefined}
            alt="modal status"
            src={this.renderImage()}
          />
          <div>
            <h4>{this.renderTitle()}</h4>
            <p>{this.renderSubtitle()}</p>
          </div>
        </main>
        {!isLoading && (
          <Footer>
            <Link to={{ pathname: "/" }} className="right">
              <button className="white" type="button">
                {isOk ? t("footer.login") : t("footer.init")}
              </button>
            </Link>
          </Footer>
        )}
      </div>
    );
  }
  renderTitle() {
    const { t } = this.props;
    if (this.props.isLoading) {
      return t("modal.loading.title");
    }
    return this.props.isOk ? t("modal.success.title") : t("modal.fail.title");
  }
  renderSubtitle() {
    const { t } = this.props;
    if (this.props.isLoading) {
      return t("modal.loading.subtitle");
    }
    return this.props.isOk
      ? t("modal.success.subtitle")
      : t("modal.fail.subtitle");
  }
  renderImage() {
    return this.props.isLoading ? loading : this.props.isOk ? success : fail;
  }
}

export default withTranslation()(FeedbackModal);
