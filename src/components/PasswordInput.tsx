import React, { useState, FunctionComponent } from "react";
import { useTranslation } from "react-i18next";
import eye from "../assets/img/eye.png";

const MIN_PASSWORD_LENGHT: number = 8;
const MAX_PASSWORD_LENGHT: number = 24;

const hasValidLength = (password: string): boolean =>
  password.length >= MIN_PASSWORD_LENGHT &&
  password.length <= MAX_PASSWORD_LENGHT;

const hasAtLeastANumber = (password: string): boolean => /\d/g.test(password);

const hasAtLeastACapitalLetter = (password: string): boolean =>
  /[A-Z]/g.test(password);

const isValidPassword = (password: string): boolean => {
  return (
    hasValidLength(password) &&
    hasAtLeastANumber(password) &&
    hasAtLeastACapitalLetter(password)
  );
};

interface PasswordInputProps {
  placeholder: string;
  title: string;
  errorLabel: string;
  value: string;
  isValid: boolean;
  isRepetitionInput?: boolean | false;
  onInput: (password: string) => void;
}

const PasswordInput: FunctionComponent<PasswordInputProps> = (
  props: PasswordInputProps
) => {
  const { t } = useTranslation();
  const [isVisible, setIsVisible] = useState(false);
  const handleToggleVisibility = () => setIsVisible(!isVisible);
  const handleUserInput = (event: React.KeyboardEvent<HTMLInputElement>) =>
    props.onInput(event.currentTarget.value);
  const handleOnPaste = (event: React.ClipboardEvent) =>
    props.isRepetitionInput && event.preventDefault();

  return (
    <section className="password">
      <h5>{props.title}</h5>
      <div>
        <input
          type={isVisible ? "text" : "password"}
          title={t("step2.password-rules")}
          placeholder={props.placeholder}
          defaultValue={props.value}
          onInput={handleUserInput}
          onPaste={handleOnPaste}
        />
        {!props.isValid && props.value.length > 0 && <hr />}
        <button type="button" onClick={handleToggleVisibility}>
          <img alt="eye icon" src={eye} />
        </button>
      </div>
      {!props.isValid && props.value.length >= MIN_PASSWORD_LENGHT && (
        <p>{props.errorLabel}</p>
      )}
    </section>
  );
};

export { isValidPassword };

export default PasswordInput;
