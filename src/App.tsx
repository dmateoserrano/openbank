import { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Step1 from "./views/Step1";
import Step2 from "./views/Step2";
import "./App.scss";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route strict exact path={"/"}>
            <Step1 />
          </Route>
          <Route strict exact path={"/form"}>
            <Step2 />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
