import * as Locale from ".";

interface i18NextInterpolation {
  escapeValue: boolean;
}

interface i18NextResource {
  [key: string]: {
    translation: typeof Locale.es;
  };
}
interface i18NextConfig {
  resources: i18NextResource;
  fallbackLng: string;
  interpolation: i18NextInterpolation;
}

const config: i18NextConfig = {
  resources: {
    es: {
      translation: Locale.es,
    },
    en: {
      translation: Locale.en,
    },
  },
  fallbackLng: "es",

  interpolation: {
    escapeValue: false,
  },
};

export default config;
