import { isValidPassword } from "../components/PasswordInput";

describe("Password validation", () => {
  it("An empty password is incorrect", () => {
    expect(isValidPassword("")).toEqual(false);
  });
  it("A password < 8 characters is incorrect", () => {
    expect(isValidPassword("aaaa")).toEqual(false);
  });
  it("A password > 24 characters is incorrect", () => {
    expect(isValidPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")).toEqual(false);
  });
  it("A password with correct length, without numbers and capital letters, is incorrect", () => {
    expect(isValidPassword("aaaaaaaa")).toEqual(false);
  });
  it("A password with correct length, with number and no capital letter, is incorrect", () => {
    expect(isValidPassword("aaaaaaa4")).toEqual(false);
  });
  it("A password with correct length, without capital letter and no number, is incorrect", () => {
    expect(isValidPassword("aaaaaaaA")).toEqual(false);
  });
  it("A password < 8 characters, despite it has a number, is incorrect", () => {
    expect(isValidPassword("aa4")).toEqual(false);
  });
  it("A password < 8 characters, despite it has a capital letter, is incorrect", () => {
    expect(isValidPassword("aaA")).toEqual(false);
  });
  it("A password < 8 characters, despite it has a number and capital letter, is incorrect", () => {
    expect(isValidPassword("a4A")).toEqual(false);
  });
  it("A password > 24 characters, despite it has a number, is incorrect", () => {
    expect(isValidPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa4")).toEqual(false);
  });
  it("A password > 24 characters, despite it has a capital letter, is incorrect", () => {
    expect(isValidPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaA")).toEqual(false);
  });
  it("A password > 24 characters, despite it has a number and capital letter, is incorrect", () => {
    expect(isValidPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa4A")).toEqual(false);
  });
  it("A password with correct length, a number and a capital letter, is correct", () => {
    expect(isValidPassword("aaaaaaA4")).toEqual(true);
  });
  it("A password with correct length, with numbers and capital letters at the beging, is correct", () => {
    expect(isValidPassword("AA44aaaa")).toEqual(true);
  });
  it("A password with correct length, with numbers and capital letters at the end, is correct", () => {
    expect(isValidPassword("aaaaAA44")).toEqual(true);
  });
  it("A password with correct length, with numbers and capital letters in the middle, is correct", () => {
    expect(isValidPassword("aaAA44aa")).toEqual(true);
  });
});
