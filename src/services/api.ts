interface FakeResponse {
  status: number;
}

const PRUEBA_KO: string = "pruebaKO123";
const RESPONSE_OK: FakeResponse = { status: 200 };
const RESPONSE_KO: FakeResponse = { status: 401 };

const submitForm = (
  pass: string,
  _repass?: string,
  _optionalQuestion?: string
) =>
  new Promise<FakeResponse>((resolve, reject) =>
    setTimeout(
      () => (pass !== PRUEBA_KO ? resolve(RESPONSE_OK) : reject(RESPONSE_KO)),
      3000
    )
  );

export { submitForm };
